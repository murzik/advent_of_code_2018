# Challenge https://adventofcode.com/2018/day/3

- First part: Create rectangles with coordinates ("claims") from the input and return all points with more than one rectangle 
- Second part: Find all claims without conflicts

# How to run

- `cd solution && cargo run example.txt`
- `cd solution && cargo run challenge.txt`