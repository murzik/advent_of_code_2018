extern crate env_logger;
extern crate solution;
extern crate structopt;

use structopt::StructOpt;

use solution::{run, Configuration};

fn main() {
    env_logger::init();
    let config = Configuration::from_args();
    run(&config);
}
