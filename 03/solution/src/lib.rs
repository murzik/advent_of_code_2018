#![warn(clippy::all, clippy::pedantic)]

extern crate regex;
extern crate structopt;
#[macro_use]
extern crate log;
#[macro_use]
extern crate proptest;
extern crate env_logger;

use std::collections::HashMap;
use std::fs::File;
use std::io::{BufRead, BufReader};
use std::path::PathBuf;
use std::process;

use regex::Regex;
use structopt::StructOpt;

/// Solver for the 3rd puzzle of the Advent of Code 2018
#[derive(Debug, StructOpt)]
pub struct Configuration {
    /// Input file
    #[structopt(name = "FILE", parse(from_os_str))]
    path: PathBuf,
}

#[derive(Debug, PartialEq, Clone)]
struct Rect {
    x: u32,
    y: u32,
    width: u32,
    height: u32,
}

#[derive(Debug, PartialEq, Clone)]
struct Claim {
    id: u32,
    rect: Rect,
}

impl Claim {
    /// Create claims by a specific input string like `#123 @ 3,2: 5x4`, which will be
    /// parsed to `Claim { id: 123, Rectangle {x: 3, y: 2, width: 5, height: 4}}`
    fn new(input: &str) -> Result<Self, &'static str> {
        debug!("Current input: {}", input);

        let re = Regex::new(r"^#(\d+)\s@\s(\d+),(\d+):\s(\d+)x(\d+)$").unwrap();
        let caps = match re.captures(input) {
            Some(c) => c,
            None => return Err("Input doesn't match format, skipping"),
        };

        // nothing to fear here
        let id: u32 = caps[1].parse().unwrap();
        let rect = Rect {
            x: caps[2].parse().unwrap(),
            y: caps[3].parse().unwrap(),
            width: caps[4].parse().unwrap(),
            height: caps[5].parse().unwrap(),
        };

        let claim = match rect {
            Rect { width: 0, .. } | Rect { height: 0, .. } => {
                return Err("Width or height may not be zero, skipping!");
            }
            _ => Self { id, rect },
        };

        debug!("Parsed '{}' to {:?}", input, &claim);
        Ok(claim)
    }

    /// Returns all occupied points of the claim
    /// Some properties of this function are:
    /// - the length of the resulting vector == width * height
    /// - all points in the resulting vector are unique
    /// - the resulting vector is sorted
    /// - the numerical difference between two successive points is
    ///   `(x1, y1) - (x0, y0) = (1, _) || (_, 1)`
    fn to_points(&self) -> Vec<(u32, u32)> {
        let mut vec = Vec::new();

        for x in (self.rect.x)..(self.rect.x + self.rect.width) {
            for y in (self.rect.y)..(self.rect.y + self.rect.height) {
                vec.push((x, y));
            }
        }

        vec
    }
}

/// Returns the total number of positions with multiple claims.
fn number_of_fields_with_conflicts(claims: &[Claim]) -> u32 {
    let mut map: HashMap<(u32, u32), u32> = HashMap::new();
    for c in claims {
        for pair in c.to_points() {
            let count = map.entry(pair).or_insert(0);
            *count += 1;
        }
    }

    map.values().map(|x| if *x > 1 { 1 } else { 0 }).sum()
}

/// Returns the ids of all claims that don't conflict. Note: The correct solution for my challenge
/// data is `222`. However, this implementation returns four values: `[941, 1252, 1249, 222]`.
fn find_claim_ids_without_conflicts(claims: &[Claim]) -> Vec<u32> {
    let mut point_to_ids_map: HashMap<(u32, u32), Vec<u32>> = HashMap::new();
    let mut id_to_size_map: HashMap<u32, u32> = HashMap::new();

    for c in claims {
        id_to_size_map.insert(c.id, c.rect.width * c.rect.width);
        for pair in c.to_points() {
            let v = point_to_ids_map.entry(pair).or_insert_with(Vec::new);
            v.push(c.id);
        }
    }

    let ids: Vec<u32> = point_to_ids_map
        .values()
        .filter(|v| v.len() == 1)
        .map(|e| e[0])
        .collect();

    let mut results: Vec<u32> = Vec::new();
    for (id, size) in id_to_size_map {
        let count: u32 = ids.iter().map(|e| if *e == id { 1 } else { 0 }).sum();

        if count == size {
            results.push(id);
        }
    }

    results
}

pub fn run(conf: &Configuration) {
    let input = File::open(&conf.path).unwrap_or_else(|e| {
        eprintln!("Could not open file {:?}: {}", &conf.path, e);
        process::exit(1);
    });

    let reader = BufReader::new(input);
    let claims: Vec<Claim> = reader
        .lines()
        .map(|line| Claim::new(line.unwrap().as_str()))
        .filter_map(|c| c.ok())
        .collect();

    println!(
        "Number of conflicts: {}",
        number_of_fields_with_conflicts(&claims)
    );

    let ids: Vec<u32> = find_claim_ids_without_conflicts(&claims);
    println!("Conflict free ids: {:?}", ids);
}

#[cfg(test)]
mod test {
    use crate::*;

    #[test]
    fn new_claim_from_str() {
        let input = "#123 @ 3,2: 5x4";
        let expected = Claim {
            id: 123,
            rect: Rect {
                x: 3,
                y: 2,
                width: 5,
                height: 4,
            },
        };

        assert_eq!(Claim::new(input).unwrap(), expected);
    }

    #[test]
    fn claim_to_points() {
        let claim = Claim::new("#123 @ 3,2: 2x2");
        let points = claim.unwrap().to_points();

        assert_eq!(points.len(), 4);
        assert_eq!(points, vec![(3, 2), (3, 3), (4, 2), (4, 3)]);
    }

    #[test]
    fn find_conflicts() {
        let claims = vec!["#1 @ 1,3: 4x4", "#2 @ 3,1: 4x4", "#3 @ 5,5: 2x2"];
        let claims: Vec<Claim> = claims
            .iter()
            .map(|s| Claim::new(s))
            .filter_map(|c| c.ok())
            .collect();

        assert_eq!(4, number_of_fields_with_conflicts(&claims));
    }

    #[test]
    fn test_find_conflict_free_claims() {
        let claims = vec!["#1 @ 1,3: 4x4", "#2 @ 3,1: 4x4", "#3 @ 5,5: 2x2"];
        let claims: Vec<Claim> = claims
            .iter()
            .map(|s| Claim::new(s))
            .filter_map(|c| c.ok())
            .collect();

        let expected: Vec<u32> = vec![3];
        assert_eq!(expected, find_claim_ids_without_conflicts(&claims));
    }

    proptest! {
        /// The number of points equals width * height
        #[test]
        fn claim_to_points_number(width in 1u32..100, height in 1u32..100) {
            let claim = Claim {
                id: 1,
                rect: Rect {
                    x: 0,
                    y: 0,
                    width,
                    height,
                }
            };

            prop_assert_eq!(height * width, claim.to_points().len() as u32);
        }
    }
}
