#![warn(clippy::all, clippy::pedantic)]
extern crate rayon;

use rayon::prelude::*;

/// Simplify input using a vector as data structure. The input is expected to consist of ASCII
/// letters with mixed cases. The simplification removes adjacent letters of the same kind with opposing
/// case such as `Aa` or `aA`. This function implements the first part of the challenge.
///
/// TODO: Implement this with a linked list and benchmark it.
pub fn simplify(input: &str) -> String {
    let mut bytes: Vec<u8> = input.as_bytes().iter().map(|&b| b).collect();
    assert!(bytes.len() > 1);

    let mut do_next_round = true;
    while do_next_round {
        do_next_round = false;
        for i in 0..bytes.len() - 1 {
            match i16::from(bytes[i]) - i16::from(bytes[i + 1]) {
                32 | -32 => {
                    bytes.remove(i);
                    bytes.remove(i);
                    do_next_round = true;

                    break;
                }
                _ => (),
            }
        }
    }

    String::from_utf8(bytes).expect("Found invalid UTF-8 :(")
}

/// Returns the length of the smallest simplification, where all occurrences of a letter
/// (case insensitive) are removed. This functions implements the second part of the challenge.
pub fn fun(input: &str) -> usize {
    let mut length = usize::max_value();

    for offset in 0..25 {
        let tmp = simplify(&remove_letter_from_string(&input, 65_u8 + offset));
        if tmp.len() < length {
            length = tmp.len()
        }
    }

    length
}

/// Does the same as `fun` but employs *potential* parallelism via rayon.
pub fn fun_parallel(input: &str) -> usize {
    let lengths: Vec<usize> = (0..25)
        .into_par_iter()
        .map(|offset| simplify(&remove_letter_from_string(&input, 65_u8 + offset as u8)).len())
        .collect();

    lengths
        .iter()
        .fold(usize::max_value(), |a, &b| std::cmp::min(a, b))
}

/// The `letter` argument must be uppercase.
fn remove_letter_from_string(s: &str, letter: u8) -> String {
    assert!(letter >= 65 && letter <= 90);

    s.bytes()
        .filter(|&b| !(b == letter || b == letter + 32))
        .map(|b| b as char)
        .collect()
}

#[cfg(test)]
mod test {
    use crate::*;

    #[test]
    fn test_solve() {
        assert_eq!(String::from("dabCBAcaDA"), simplify("dabAcCaCBAcCcaDA"));
    }

    #[test]
    fn test_remove_letter_from_string() {
        let input = String::from("abcdABCD");
        assert_eq!(
            String::from("bcdBCD"),
            remove_letter_from_string(&input, 'A' as u8)
        );
    }

    #[test]
    fn test_fun() {
        assert_eq!("daDA".len(), fun("dabAcCaCBAcCcaDA"));
    }

    #[test]
    fn test_fun_parallel() {
        assert_eq!("daDA".len(), fun_parallel("dabAcCaCBAcCcaDA"));
    }
}
