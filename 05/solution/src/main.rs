extern crate clap;
extern crate solution;

use clap::{App, Arg};
use solution::{fun_parallel, simplify};

fn main() {
    let arguments = App::new("AoC Day 5")
        .arg(
            Arg::with_name("file")
                .short("f")
                .long("file")
                .takes_value(true)
                .help("challenge to solve"),
        )
        .get_matches();

    let file = arguments.value_of("file").expect("Could not open file!");
    let file_content = std::fs::read_to_string(file).unwrap();

    assert!(file_content.is_ascii());

    // solve part one
    let simplification = simplify(&file_content);
    dbg!(&simplification.len() - 1); // -1 because of the newline character

    // solve part two
    dbg!(fun_parallel(&file_content) - 1); // -1 because of the newline character
}
