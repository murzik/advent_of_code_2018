#[macro_use]
extern crate criterion;
extern crate solution;

use criterion::Criterion;
use solution::*;

fn fun_seq(c: &mut Criterion) {
    c.bench_function("fun sequential", |b| {
        b.iter(|| fun("dabAcCaCBAcCcaDAdabAcCaCBAcCcaDAdabAcCaCBAcCcaDAdabAcCaCBAcCcaDAdabAcCaCBAcCcaDAdabAcCaCBAcCcaDAdabAcCaCBAcCcaDAdabAcCaCBAcCcaDAdabAcCaCBAcCcaDA"))
    });
}

fn fun_rayon(c: &mut Criterion) {
    c.bench_function("fun rayon", |b| {
        b.iter(|| fun_parallel("dabAcCaCBAcCcaDAdabAcCaCBAcCcaDAdabAcCaCBAcCcaDAdabAcCaCBAcCcaDAdabAcCaCBAcCcaDAdabAcCaCBAcCcaDAdabAcCaCBAcCcaDAdabAcCaCBAcCcaDAdabAcCaCBAcCcaDA"))
    });
}

criterion_group!(benches, fun_seq, fun_rayon);
criterion_main!(benches);
