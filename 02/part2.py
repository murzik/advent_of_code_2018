#!/bin/env python3

with open("input.txt", mode="r") as fin:
    lines = fin.read().split("\n")

    for i, lineA in enumerate(lines):
        for j, lineB in enumerate(lines):
            if (i == j or len(lineA) != len(lineB)):
                continue;
            
            length = len(lineA)
            diff = 0
            for k in range(length):
                if (lineA[k] != lineB[k]):
                    diff += 1
                    if (diff > 1):
                        break;

            if (diff == 1):
                for k in range(length):
                    if (lineA[k] == lineB[k]):
                        print(lineA[k], end="")
                print();
                exit(0)
