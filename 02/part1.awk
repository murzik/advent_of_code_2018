#!/bin/awk -f

function countFrequencies(record, map)
{
    for (i = 1; i <= 26; i++)
    {
        map[$i]++;
    }
}

function hasCountN(map, n)
{
    for (key in map)
    {
        if (map[key] == n)
        {
            return 1;
        }
    }

    return 0;
}

BEGIN {FS = ""}

{
    delete map

    countFrequencies($0, map);
    if (hasCountN(map, 3))
    {
        three++
    }

    if (hasCountN(map, 2))
    {
        two++
    }
}

END {print "Checksum: " three * two}
