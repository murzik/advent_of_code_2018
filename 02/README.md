# Challenge https://adventofcode.com/2018/day/2

# How to run

| command                 | output                      |
| ----------------------- | --------------------------- |
| `./part1.awk test.txt`  | `12`                        |
| `./part1.awk input.txt` | `5658`                      |
| `./part2.py`            | `nmgyjkpruszlbaqwficavxneo` |
