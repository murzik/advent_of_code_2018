This repository contains my solutions for https://adventofcode.com/2018.

My goal is to practise and explore languages I enjoy. Each challenge consists of two puzzles. The idea is to authenticate via e.g. GitHub and get individual puzzle data. Once the solution of the first part is submitted successfully, the second (and more challenging) part will be unlocked.