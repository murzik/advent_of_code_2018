# Challenge https://adventofcode.com/2018/day/1

- First part: Sum all frequency changes (represented by signed integers) and print the final result
- Second part: Sum all frequency changes repeatedly until a frequency is reached twice

# How to run

| command       | argument: `test.txt` | argument: `input.txt` |
| ------------- | -------------------- | --------------------- |
| `./part1.awk` | 3                    | 406                   |
| `./part2.awk` | 2                    | 312                   |
