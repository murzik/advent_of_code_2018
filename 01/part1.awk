#!/bin/awk -f

# The following action is executed for each record (input line). Variables are
# initialized to zero. Strings are converted automatically. $1 refers to the 
# first field ("column") of a record.
{n += $1}

# This is executed after the last record.
END {print n}
