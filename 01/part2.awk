#!/bin/awk -f
BEGIN {
    n = 0;
    frequencies[n] = 1;
}

{
    n += $1;
    frequencies[n]++;
    if (frequencies[n] == 2)
    {
        print "Frequency: " n;
        exit 0;
    };
}

ENDFILE {
    ARGV[ARGC++] = ARGV[ARGC-1]
}
